export const authInterceptor = {
  request: {
    onFulfilled: null,
    onRejected: null,
  },

  response: {
    onFulfilled: (response) => {
      if (response.error && response.error.statusCode === 401) {
        localStorage.removeItem('token');
        window.location.reload();
      }
      return response;
    },
    onRejected: null,
  },
};
