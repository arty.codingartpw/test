import { axios } from './axios';

export const httpClient = {
  get(url, data) {
    return axios.get(url, { params: data });
  },

  post(url, data) {
    return axios.post(url, data);
  },

  put(url, data) {
    return axios.put(url, data);
  },

  patch(url, data) {
    return axios.patch(url, data);
  },

  delete(url) {
    return axios.delete(url);
  },
};
