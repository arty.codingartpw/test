import { httpClient } from './httpClient';

export const authApi = {
  tokenCreate(email, password) {
    return httpClient.post('/token/', { email, password });
  },

  tokenRefresh(token) {
    return httpClient.post('/token/refresh/', { token });
  },

  tokenVerify(token) {
    return httpClient.post('/token/verify/', { token });
  },
};
