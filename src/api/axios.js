import Axios from 'axios';
import { authInterceptor } from './interceptors/auth-interceptor';

const config = {
  baseURL: `${process.env.REACT_APP_API_URL}`,
  withCredentials: true,
};

export const axios = Axios.create(config);

axios.interceptors.request
  .use(authInterceptor.request.onFulfilled, authInterceptor.request.onRejected);
axios.interceptors.response
  .use(authInterceptor.response.onFulfilled, authInterceptor.response.onRejected);
