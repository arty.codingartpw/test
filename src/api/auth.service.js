import { authApi } from './auth.api';

export const authService = {
  logout() {
    localStorage.removeItem('token');
  },

  login(email, password) {
    return authApi.tokenCreate(email, password)
      .then(({ data }) => {
        this.saveUser(data.access);
        return data;
      });
  },

  checkLogin() {
    const token = localStorage.getItem('token');

    return authApi.tokenVerify(token);
  },

  hasToken() {
    return !!localStorage.getItem('token');
  },

  saveUser(token) {
    localStorage.setItem('token', token);
  },
};
