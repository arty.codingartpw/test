import { companyApi } from './company.api';

export const companyService = {
  companiesList(page) {
    return companyApi.companiesList(page);
  },

  companyCreate(fields) {
    return companyApi.companyCreate(fields);
  },

  companyRead(id) {
    return companyApi.companyRead(id);
  },

  companyUpdate(id, fields) {
    return companyApi.companyUpdate(id, fields);
  },

  companyDelete(id) {
    return companyApi.companyDelete(id);
  },

  companyBankDetailsCreate(companyPk, fields) {
    return companyApi.companyBankDetailsCreate(companyPk, fields);
  },

  companyBankDetailsDelete(companyPk, id) {
    return companyApi.companyBankDetailsDelete(companyPk, id);
  },

  companyBankDetailsUpdate(companyPk, id, fields) {
    return companyApi.companyBankDetailsUpdate(companyPk, id, fields);
  },
};
