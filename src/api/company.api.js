import { httpClient } from './httpClient';

export const companyApi = {
  companiesList(page = 0) {
    return httpClient.get('/companies/', { page: page + 1 });
  },

  companyCreate(fields) {
    return httpClient.post('/companies/', fields);
  },

  companyRead(id) {
    return httpClient.get(`/companies/${id}/`);
  },

  companyUpdate(id, fields) {
    return httpClient.put(`/companies/${id}/`, fields);
  },

  companyDelete(id) {
    return httpClient.delete(`/companies/${id}/`);
  },

  companyBankDetailsCreate(companyPk, fields) {
    return httpClient.post(`/companies/${companyPk}/bank_details/`, fields);
  },

  companyBankDetailsDelete(companyPk, id) {
    return httpClient.delete(`/companies/${companyPk}/bank_details/${id}/`);
  },

  companyBankDetailsUpdate(companyPk, id, fields) {
    return httpClient.put(`/companies/${companyPk}/bank_details/${id}/`, fields);
  },
};
