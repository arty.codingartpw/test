import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  IconButton,
} from '@material-ui/core';
import {
  Delete as DeleteIcon,
  Edit as EditIcon,
} from '@material-ui/icons';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 440,
  },
});

export function ClientsTable({
  list, count, page, changePage, onDelete,
}) {
  const classes = useStyles();

  const handleChangePage = (_, newPage) => {
    changePage(newPage);
  };

  return (
    <Paper className={classes.root}>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              <TableCell>
                Наименование компании
              </TableCell>
              <TableCell>
                Тип юр.лица
              </TableCell>
              <TableCell>
                Регион
              </TableCell>
              <TableCell>
                Город
              </TableCell>
              <TableCell />
            </TableRow>
          </TableHead>
          <TableBody>
            {list.map((item) => (
              <TableRow hover role="checkbox" tabIndex={-1} key={item.id}>
                <TableCell>
                  {item.name ?? 'N/A'}
                </TableCell>
                <TableCell>
                  {item.registered_type}
                </TableCell>
                <TableCell>
                  {item.region}
                </TableCell>
                <TableCell>
                  {item.city}
                </TableCell>

                <TableCell align="right">
                  <IconButton component={Link} to={`/client/${item.id}`}>
                    <EditIcon />
                  </IconButton>
                  <IconButton onClick={() => onDelete(item)}>
                    <DeleteIcon />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[]}
        component="div"
        count={count}
        page={page}
        rowsPerPage={30}
        onChangePage={handleChangePage}
      />
    </Paper>
  );
}

ClientsTable.defaultProps = {
  list: [],
};
ClientsTable.propTypes = {
  list: PropTypes.arrayOf(PropTypes.object),
  count: PropTypes.number.isRequired,
  page: PropTypes.number.isRequired,
  changePage: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};
