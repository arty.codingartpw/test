import React from 'react';
import {
  Box, Typography, TextField, Button, FormControlLabel, Switch, Grid,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import { useForm, Controller } from 'react-hook-form';

export function FormEditClient(props) {
  const {
    register, handleSubmit, errors, control,
  } = useForm({ defaultValues: props.initialFields });
  const onSubmit = (data) => props.onSubmit(data);

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Grid container spacing={5} justify="space-between">
        <Grid item md={6} xs={12}>
          <Typography variant="h5" component="p">
            Основная информация
          </Typography>

          <Box mt={5}>
            <TextField
              inputRef={register({ required: 'Поле обязательное' })}
              name="name"
              label="Наименование компании"
              variant="outlined"
              fullWidth
              error={!!errors.name}
              helperText={errors.name?.message}
            />
          </Box>

          <Box mt={2}>
            <Grid container spacing={4}>
              <Grid item md={6} xs={12}>
                <TextField
                  inputRef={register({ required: 'Поле обязательное' })}
                  name="shortname"
                  label="Короткое название"
                  variant="outlined"
                  fullWidth
                  error={!!errors.shortname}
                  helperText={errors.shortname?.message}
                />
              </Grid>

              <Grid item md={6} xs={12}>
                <TextField
                  inputRef={register({ required: 'Поле обязательное' })}
                  name="workscope"
                  label="Сфера деятельности"
                  variant="outlined"
                  fullWidth
                  error={!!errors.workscope}
                  helperText={errors.workscope?.message}
                />
              </Grid>
            </Grid>

            <Grid container spacing={4}>
              <Grid item md={6} xs={12}>
                <TextField
                  inputRef={register({ required: 'Поле обязательное' })}
                  name="region"
                  label="Регион"
                  variant="outlined"
                  fullWidth
                  error={!!errors.region}
                  helperText={errors.region?.message}
                />
              </Grid>

              <Grid item md={6} xs={12}>
                <TextField
                  inputRef={register({ required: 'Поле обязательное' })}
                  name="city"
                  label="Город"
                  variant="outlined"
                  fullWidth
                  error={!!errors.city}
                  helperText={errors.city?.message}
                />
              </Grid>
            </Grid>

            <Grid container spacing={4}>
              <Grid item md={6} xs={12}>
                <TextField
                  inputRef={register({ required: 'Поле обязательное' })}
                  name="email"
                  label="Email"
                  variant="outlined"
                  fullWidth
                  error={!!errors.email}
                  helperText={errors.email?.message}
                />
              </Grid>

              <Grid item md={6} xs={12}>
                <TextField
                  inputRef={register({ required: 'Поле обязательное' })}
                  name="phone"
                  label="Телефон"
                  variant="outlined"
                  fullWidth
                  error={!!errors.phone}
                  helperText={errors.phone?.message}
                />
              </Grid>
            </Grid>

            <Box marginTop="15px">
              <TextField
                inputRef={register({ required: 'Поле обязательное' })}
                name="description"
                label="Дополнительно (описание)"
                type="text"
                variant="outlined"
                fullWidth
                multiline
                rows={4}
                error={!!errors.description}
                helperText={errors.description?.message}
              />
            </Box>
          </Box>
        </Grid>

        <Grid item md={6} xs={12}>
          <Typography variant="h5" component="p">
            Реквизиты компании
          </Typography>

          <Box mt={5}>
            <TextField
              inputRef={register({ required: 'Поле обязательное' })}
              name="registered_name"
              label="Наименование юр.лица"
              variant="outlined"
              fullWidth
              error={!!errors.registered_name}
              helperText={errors.registered_name?.message}
            />
          </Box>

          <Box mt={2}>
            <Grid container spacing={4}>
              <Grid item md={6} xs={12}>
                <TextField
                  inputRef={register({ required: 'Поле обязательное' })}
                  name="registered_type"
                  label="Тип юр.лица"
                  variant="outlined"
                  fullWidth
                  error={!!errors.registered_type}
                  helperText={errors.registered_type?.message}
                />
              </Grid>

              <Grid item md={6} xs={12}>
                <TextField
                  inputRef={register({ required: 'Поле обязательное' })}
                  name="bin_iin"
                  label="БИН/ИИН"
                  variant="outlined"
                  fullWidth
                  error={!!errors.bin_iin}
                  helperText={errors.bin_iin?.message}
                />
              </Grid>
            </Grid>

            <Grid container spacing={4}>
              <Grid item md={6} xs={12}>
                <TextField
                  inputRef={register({ required: 'Поле обязательное' })}
                  name="leader"
                  label="Руководитель"
                  variant="outlined"
                  fullWidth
                  error={!!errors.leader}
                  helperText={errors.leader?.message}
                />
              </Grid>

              <Grid item md={6} xs={12}>
                <TextField
                  inputRef={register({ required: 'Поле обязательное' })}
                  name="leader_position"
                  label="Должность руководителя"
                  variant="outlined"
                  fullWidth
                  error={!!errors.leader_position}
                  helperText={errors.leader_position?.message}
                />
              </Grid>
            </Grid>

            <Grid container spacing={4}>
              <Grid item md={6} xs={12}>
                <TextField
                  inputRef={register({ required: 'Поле обязательное' })}
                  name="registered_address"
                  label="Юридический адрес"
                  variant="outlined"
                  fullWidth
                  error={!!errors.registered_address}
                  helperText={errors.registered_address?.message}
                />
              </Grid>

              <Grid item md={6} xs={12}>
                <TextField
                  inputRef={register({ required: 'Поле обязательное' })}
                  name="address"
                  label="Фактический адрес"
                  variant="outlined"
                  fullWidth
                  error={!!errors.address}
                  helperText={errors.address?.message}
                />
              </Grid>
            </Grid>

            <Box marginTop="15px">
              <Controller
                control={control}
                name="tax_payer"
                render={({
                  onChange, onBlur, value, name,
                }) => (
                  <FormControlLabel
                    control={(
                      <Switch
                        onBlur={onBlur}
                        onChange={(e) => onChange(e.target.checked)}
                        color="primary"
                        checked={!!value}
                        name={name}
                      />
                    )}
                    label="Плательщик НДС (нет/да)"
                    labelPlacement="start"
                  />
                )}
              />
            </Box>
          </Box>
        </Grid>
      </Grid>

      <Box display="flex" justifyContent="flex-end" mt={6}>
        <Button variant="contained" color="primary" type="submit" disabled={props.isLoading}>
          Сохранить
        </Button>
      </Box>
    </form>
  );
}

FormEditClient.defaultProps = {
  isLoading: false,
};

FormEditClient.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  initialFields: PropTypes.objectOf(PropTypes.any).isRequired,
  isLoading: PropTypes.bool,
};
