import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import {
  Box, Typography, TextField, Button, InputAdornment, IconButton,
} from '@material-ui/core';
import {
  Visibility,
  VisibilityOff,
} from '@material-ui/icons';
import PropTypes from 'prop-types';

export function FormLogin(props) {
  const [showPassword, setShowPassword] = useState(false);
  const handleClickShowPassword = () => setShowPassword(!showPassword);

  const {
    register, handleSubmit, errors,
  } = useForm();
  const onSubmit = (data) => props.onSubmit(data);

  return (
    <Box component="form" boxShadow={2} p={4} width="400px" maxWidth="100%" onSubmit={handleSubmit(onSubmit)}>
      <Typography variant="h5" component="h1">
        Авторизация
      </Typography>

      <Box mt={5}>
        <TextField
          inputRef={register({ required: 'Поле обязательное' })}
          name="email"
          label="Email"
          variant="outlined"
          fullWidth
          error={!!errors.email}
          helperText={errors.email?.message}
        />

        <Box mt={3}>
          <TextField
            inputRef={register({ required: 'Поле обязательное' })}
            name="password"
            label="Password"
            type={showPassword ? 'text' : 'password'}
            variant="outlined"
            fullWidth
            error={!!errors.password}
            helperText={errors.password?.message}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="Переключить видимость пароля"
                    onClick={handleClickShowPassword}
                  >
                    {showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
        </Box>

        <Box display="flex" justifyContent="flex-end" mt={3}>
          <Button variant="contained" color="primary" type="submit" disabled={props.isLoading}>
            Войти
          </Button>
        </Box>
      </Box>
    </Box>
  );
}

FormLogin.defaultProps = {
  isLoading: false,
};

FormLogin.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
};
