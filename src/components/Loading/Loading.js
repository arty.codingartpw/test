import React from 'react';
import { Box, CircularProgress } from '@material-ui/core';

export function Loading() {
  return (
    <Box display="flex" justifyContent="center" alignItems="center" height="100%">
      <CircularProgress />
    </Box>
  );
}
