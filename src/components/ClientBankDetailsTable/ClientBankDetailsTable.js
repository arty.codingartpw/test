import React, { forwardRef, useState } from 'react';
import MaterialTable from 'material-table';
import {
  AddBox,
  ArrowDownward,
  Check,
  ChevronLeft,
  ChevronRight,
  Clear,
  DeleteOutline,
  Edit,
  FilterList,
  FirstPage,
  LastPage,
  Remove,
  SaveAlt,
  Search,
  ViewColumn,
} from '@material-ui/icons/';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';
import { companyService } from '../../api/company.service';

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
};

export function ClientBankDetailsTable({ companyPk, initialFields, onSubmit }) {
  const [fields, setFields] = useState(initialFields);

  const columns = [
    {
      title: 'Банк',
      field: 'bank',
      validate: (rowData) => !!rowData.bank,
    },
    {
      title: 'БИК',
      field: 'bank_id_code',
      validate: (rowData) => !!rowData.bank_id_code,
    },
    {
      title: 'Номер счёта',
      field: 'account_number',
      validate: (rowData) => !!rowData.account_number,
    },
    {
      title: 'Валюта',
      field: 'currency',
      lookup: { usd: 'USD', kzt: 'KZT', rub: 'RUB' },
      validate: (rowData) => !!rowData.currency,
    },
  ];

  const onRowAdd = (newData) => (
    companyService.companyBankDetailsCreate(companyPk, newData)
      .then(({ data }) => {
        setFields([...fields, data]);
        onSubmit([...fields, data]);
      })
      .catch(() => toast.error('Произошла ошибка при добавлении, попробуйте ещё раз'))
  );

  const onRowUpdate = (newData) => (
    companyService.companyBankDetailsUpdate(companyPk, newData.id, newData)
      .then(({ data }) => {
        const newFields = fields.filter((item) => item.id !== newData.id);

        setFields([...newFields, data]);
        onSubmit([...newFields, data]);
      })
      .catch(() => toast.error('Произошла ошибка при обновлении, попробуйте ещё раз'))
  );

  const onRowDelete = (oldData) => (
    companyService.companyBankDetailsDelete(companyPk, oldData.id)
      .then(() => {
        const newFields = fields.filter((item) => item.id !== oldData.id);

        setFields(newFields);
        onSubmit(fields);
      })
      .catch(() => toast.error('Произошла ошибка при удалении, попробуйте ещё раз'))
  );

  return (
    <MaterialTable
      localization={{
        body: {
          emptyDataSourceMessage: 'Нет записей для отображения',
          addTooltip: 'Добавить',
          deleteTooltip: 'Удалить',
          editTooltip: 'Редактировать',
          filterRow: {
            filterTooltip: 'Фильтр',
          },
          editRow: {
            deleteText: 'Вы хотите удалить эту строку?',
            cancelTooltip: 'Отменить',
            saveTooltip: 'Запись',
          },
        },
        grouping: {
          placeholder: 'Потяните за заголовок ...',
          groupedBy: 'Группа по:',
        },
        header: {
          actions: '',
        },
        pagination: {
          labelDisplayedRows: '{from}-{to} из {count}',
          labelRowsSelect: 'Линии',
          labelRowsPerPage: 'Строк на странице:',
          firstAriaLabel: 'Первая страница',
          firstTooltip: 'Первая страница',
          previousAriaLabel: 'Предыдущая страница',
          previousTooltip: 'Предыдущая страница',
          nextAriaLabel: 'Следующая страница',
          nextTooltip: 'Следующая страница',
          lastAriaLabel: 'Предыдущая страница',
          lastTooltip: 'Предыдущая страница',
        },
        toolbar: {
          addRemoveColumns: 'Добавить или удалить столбцы',
          nRowsSelected: '{0} выбранная строка(и)',
          showColumnsTitle: 'Просмотр столбцов',
          showColumnsAriaLabel: 'Просмотр столбцов',
          exportTitle: 'Экспорт',
          exportAriaLabel: 'Экспорт',
          exportName: 'Экспорт в CSV',
          searchTooltip: 'Поиск',
          searchPlaceholder: 'Поиск',
        },
      }}
      icons={tableIcons}
      title="Банковские реквизиты компании"
      columns={columns}
      data={fields}
      onCellEditFinished={() => {}}
      editable={{
        onRowAdd,
        onRowUpdate,
        onRowDelete,
      }}
    />
  );
}

ClientBankDetailsTable.propTypes = {
  initialFields: PropTypes.arrayOf(PropTypes.any).isRequired,
  companyPk: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
};
