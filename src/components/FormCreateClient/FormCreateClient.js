import React from 'react';
import { useForm } from 'react-hook-form';
import {
  Box, Button, TextField, Grid,
} from '@material-ui/core';
import PropTypes from 'prop-types';

export function FormCreateClient(props) {
  const {
    register, handleSubmit, errors,
  } = useForm();
  const onSubmit = (data) => props.onSubmit(data);

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Grid container spacing={4}>
        <Grid item md={6} xs={12}>
          <TextField
            inputRef={register({ required: 'Поле обязательное' })}
            name="name"
            label="Наименование компании"
            variant="outlined"
            fullWidth
            error={!!errors.name}
            helperText={errors.name?.message}
          />
        </Grid>
        <Grid item md={6} xs={12}>
          <TextField
            inputRef={register({ required: 'Поле обязательное' })}
            name="shortname"
            label="Краткое описание"
            variant="outlined"
            fullWidth
            error={!!errors.shortname}
            helperText={errors.shortname?.message}
          />
        </Grid>
      </Grid>

      <Grid container spacing={4}>
        <Grid item md={6} xs={12}>
          <TextField
            inputRef={register({ required: 'Поле обязательное' })}
            name="registered_type"
            label="Тип юр.лица"
            variant="outlined"
            fullWidth
            error={!!errors.registered_type}
            helperText={errors.registered_type?.message}
          />
        </Grid>
        <Grid item md={6} xs={12}>
          <TextField
            inputRef={register({ required: 'Поле обязательное' })}
            name="workscope"
            label="Сфера деательности"
            variant="outlined"
            fullWidth
            error={!!errors.workscope}
            helperText={errors.workscope?.message}
          />
        </Grid>
      </Grid>

      <Grid container spacing={4}>
        <Grid item md={6} xs={12}>
          <TextField
            inputRef={register({ required: 'Поле обязательное' })}
            name="region"
            label="Регион"
            variant="outlined"
            fullWidth
            error={!!errors.region}
            helperText={errors.region?.message}
          />
        </Grid>
        <Grid item md={6} xs={12}>
          <TextField
            inputRef={register({ required: 'Поле обязательное' })}
            name="city"
            label="Город"
            variant="outlined"
            fullWidth
            error={!!errors.city}
            helperText={errors.city?.message}
          />
        </Grid>
      </Grid>

      <Grid container spacing={4}>
        <Grid item md={6} xs={12}>
          <TextField
            inputRef={register({ required: 'Поле обязательное' })}
            name="email"
            label="Email"
            type="email"
            variant="outlined"
            fullWidth
            error={!!errors.email}
            helperText={errors.email?.message}
          />
        </Grid>
        <Grid item md={6} xs={12}>
          <TextField
            inputRef={register({ required: 'Поле обязательное' })}
            name="phone"
            label="Телефон"
            type="text"
            variant="outlined"
            fullWidth
            error={!!errors.phone}
            helperText={errors.phone?.message}
          />
        </Grid>
      </Grid>

      <Box marginTop="15px">
        <TextField
          inputRef={register({ required: 'Поле обязательное' })}
          name="description"
          label="Дополнительно (описание)"
          type="text"
          variant="outlined"
          fullWidth
          multiline
          rows={4}
          error={!!errors.description}
          helperText={errors.description?.message}
        />
      </Box>

      <Box display="flex" justifyContent="flex-end" mt={3}>
        <Button
          variant="contained"
          color="primary"
          type="submit"
          disabled={props.isLoading}
        >
          Добавить
        </Button>
      </Box>
    </form>
  );
}

FormCreateClient.defaultProps = {
  isLoading: false,
};

FormCreateClient.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
};
