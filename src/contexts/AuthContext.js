import React from 'react';

export const AuthContext = React.createContext(null);
AuthContext.displayName = 'authContext';

export const AuthProvider = AuthContext.Provider;
export const AuthConsumer = AuthContext.Consumer;
