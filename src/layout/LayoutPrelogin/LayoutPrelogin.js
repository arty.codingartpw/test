import React from 'react';
import PropTypes from 'prop-types';
import { Container } from '@material-ui/core';

export function LayoutPrelogin({ children }) {
  return (
    <Container>
      {children}
    </Container>
  );
}

LayoutPrelogin.propTypes = {
  children: PropTypes.node.isRequired,
};
