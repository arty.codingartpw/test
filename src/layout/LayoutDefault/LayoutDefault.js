import React, { useContext, useState } from 'react';
import {
  ListItemText,
  AppBar,
  CssBaseline,
  Drawer,
  Hidden,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  makeStyles,
  Toolbar,
  Typography,
  Box,
  Button,
  Dialog,
  DialogContent,
  DialogTitle,
} from '@material-ui/core';
import * as colors from '@material-ui/core/colors';
import {
  Menu as MenuIcon,
  Add as AddIcon,
  ExitToApp as ExitToAppIcon,
  Assignment as AssignmentIcon,
} from '@material-ui/icons';
import { Link, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';
import { FormCreateClient } from '../../components/FormCreateClient';
import { AuthContext } from '../../contexts/AuthContext';
import { companyApi } from '../../api/company.api';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('md')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    backgroundColor: colors.common.white,
    color: colors.common.black,

    [theme.breakpoints.up('md')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
  },
  toolbar: theme.mixins.toolbar,
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    maxWidth: '100%',
  },
  selected: {
    backgroundColor: colors.blue[400],
  },
  danger: {
    color: colors.red[400],
  },
}));

function ListItemLink({ to, children }) {
  const classes = useStyles();

  return (
    <Route path={to}>
      {({ match }) => (
        <ListItem
          component={Link}
          to={to}
          selected={match.isExact}
          classes={{ selected: classes.selected }}
        >
          {children}
        </ListItem>
      )}
    </Route>
  );
}

ListItemLink.propTypes = {
  to: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

function DrawerContent() {
  const { logout } = useContext(AuthContext);
  const classes = useStyles();

  return (
    <>
      <Box padding="20px">
        <img src="/logotype.svg" alt="Логотип" />
      </Box>

      <Box>
        <List>
          <ListItemLink to="/">
            <ListItemIcon><AssignmentIcon /></ListItemIcon>
            <ListItemText primary="Клиенты" />
          </ListItemLink>
          <ListItem button onClick={logout}>
            <ListItemIcon className={classes.danger}><ExitToAppIcon /></ListItemIcon>
            <ListItemText className={classes.danger} primary="Выход" />
          </ListItem>
        </List>
      </Box>
    </>
  );
}

function DialogCreateClient() {
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const [loading, setLoading] = useState(false);
  const createCompany = (fields) => {
    setLoading(true);

    companyApi.companyCreate(fields)
      .then(() => {
        handleClose();
        toast.success('Клиент успешно создан');
      })
      .catch(() => {
        toast.error('Не удалось создать клиента');
        setLoading(false);
      });
  };

  return (
    <>
      <Button
        variant="contained"
        color="primary"
        startIcon={<AddIcon />}
        onClick={handleClickOpen}
      >
        Добавить
      </Button>
      <Dialog fullWidth maxWidth="sm" onClose={handleClose} open={open}>
        <DialogTitle onClose={handleClose}>
          Добавить клиента
        </DialogTitle>
        <DialogContent dividers>
          <FormCreateClient onSubmit={createCompany} isLoading={loading} />
        </DialogContent>
      </Dialog>
    </>
  );
}

export function LayoutDefault({ children }) {
  const classes = useStyles();
  const [mobileOpen, setMobileOpen] = useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="Открыть меню"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h5" noWrap>
            Клиенты
          </Typography>

          <Box marginLeft="auto">
            <DialogCreateClient />
          </Box>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer}>
        <Hidden mdUp implementation="css">
          <Drawer
            variant="temporary"
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true,
            }}
          >
            <DrawerContent />
          </Drawer>
        </Hidden>
        <Hidden smDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            <DrawerContent />
          </Drawer>
        </Hidden>
      </nav>
      <main className={classes.content}>
        <div className={classes.toolbar} />

        {children}
      </main>
    </div>
  );
}

LayoutDefault.propTypes = {
  children: PropTypes.node.isRequired,
};

LayoutDefault.propTypes = {
  children: PropTypes.node.isRequired,
};
