import React, { useState, useEffect } from 'react';
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom';
import { Box } from '@material-ui/core';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { ruRU } from '@material-ui/core/locale';
import { LayoutPrelogin, LayoutDefault } from './layout';
import {
  PageClients, PageNotFound, PageLogin, PageClient,
} from './pages';
import { Loading } from './components/Loading';
import { AuthProvider } from './contexts/AuthContext';
import { authService } from './api/auth.service';

const theme = createMuiTheme({}, ruRU);

function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [pageLoaded, setPageLoaded] = useState(false);

  const logout = () => {
    setIsLoggedIn(false);
    authService.logout();
  };

  useEffect(() => {
    if (!authService.hasToken()) {
      setPageLoaded(true);
      return;
    }

    authService.checkLogin()
      .then(() => {
        setIsLoggedIn(true);
      })
      .catch(() => {})
      .finally(() => setPageLoaded(true));
  }, []);

  let body = null;
  if (!pageLoaded) {
    body = (
      <Box height="100vh">
        <Loading />
      </Box>
    );
  } else if (!isLoggedIn) {
    body = (
      <LayoutPrelogin>
        <PageLogin />
      </LayoutPrelogin>
    );
  } else {
    body = (
      <Switch>
        <Route exact path="/">
          <LayoutDefault>
            <PageClients />
          </LayoutDefault>
        </Route>

        <Route path="/client/:id">
          <LayoutDefault>
            <PageClient />
          </LayoutDefault>
        </Route>

        <Route>
          <LayoutDefault>
            <PageNotFound />
          </LayoutDefault>
        </Route>
      </Switch>
    );
  }

  return (
    <Router>
      <AuthProvider value={{ isLoggedIn, setIsLoggedIn, logout }}>
          <ThemeProvider theme={theme}>
            {body}
          </ThemeProvider>
        <ToastContainer />
      </AuthProvider>
    </Router>
  );
}

export default App;
