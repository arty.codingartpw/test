import React, {
  useState, useEffect, useCallback, useMemo,
} from 'react';
import {
  Box, TextField, Grid, MenuItem, IconButton,
} from '@material-ui/core';
import { Refresh as RefreshIcon } from '@material-ui/icons';
import { toast } from 'react-toastify';
import { ClientsTable, Loading } from '../../components';
import { companyService } from '../../api/company.service';

export function PageClients() {
  const [loaded, setLoaded] = useState(false);
  const [companies, setCompanies] = useState({ results: [], count: 0 });
  const [companyPage, setCompanyPage] = useState(0);
  const [selectRegion, setSelectRegion] = useState('');
  const [selectType, setSelectType] = useState('');
  const [fieldNameCompany, setFieldNameCompany] = useState('');
  const [fieldCity, setFieldCity] = useState('');

  const handleChangePage = (page) => {
    setCompanyPage(page);
    setSelectRegion('');
    setSelectType('');
    setFieldNameCompany('');
    setFieldCity('');
  };

  const getCompaniesList = useCallback(() => {
    setLoaded(false);

    companyService.companiesList(companyPage)
      .then(({ data }) => setCompanies(data))
      .catch(() => {})
      .finally(() => setLoaded(true));
  }, [companyPage]);
  useEffect(() => getCompaniesList(), [getCompaniesList]);

  const deleteCompany = (company) => {
    companyService.companyDelete(company.id)
      .then(() => {
        getCompaniesList();
        toast.success('Клиент успешно удалён');
      })
      .catch(() => {
        toast.error('Не удалось удалить клиента');
      });
  };

  const filterListByRegion = useMemo(() => {
    const list = new Set();

    companies.results.forEach((item) => item.region && list.add(item.region));

    return Array.from(list);
  }, [companies]);
  const filterListByType = useMemo(() => {
    const list = new Set();

    companies.results.forEach((item) => item.registered_type && list.add(item.registered_type));

    return Array.from(list);
  }, [companies]);

  const filterByString = (value, field, list) => list
    .filter((item) => String(item[field]).toUpperCase().indexOf(value.toUpperCase()) > -1);
  const filterBySelect = (value, field, list) => list
    .filter((item) => item[field] === value);
  const companiesFiltered = useMemo(() => {
    let filtered = companies.results;

    if (fieldNameCompany !== '') {
      filtered = filterByString(fieldNameCompany, 'name', filtered);
    }
    if (selectType !== '') {
      filtered = filterBySelect(selectType, 'registered_type', filtered);
    }
    if (selectRegion !== '') {
      filtered = filterBySelect(selectRegion, 'region', filtered);
    }
    if (fieldCity !== '') {
      filtered = filterByString(fieldCity, 'city', filtered);
    }
    return filtered;
  }, [fieldNameCompany, selectRegion, selectType, fieldCity, companies]);

  return !loaded ? <Loading /> : (
    <>
      <Box mb={4} display="flex" alignItems="center">
        <Grid container spacing={2}>
          <Grid item md={3} xs={12}>
            <TextField
              label="Наименование компании"
              variant="outlined"
              fullWidth
              size="small"
              onChange={(event) => setFieldNameCompany(event.target.value)}
            />
          </Grid>
          <Grid item md={3} xs={12}>
            <TextField
              select
              fullWidth
              size="small"
              label="Select"
              variant="outlined"
              value={selectType}
              onChange={(event) => setSelectType(event.target.value)}
            >
              <MenuItem value="">
                Без фильтра
              </MenuItem>
              {filterListByType.map((item) => (
                <MenuItem value={item} key={item}>
                  {item}
                </MenuItem>
              ))}
            </TextField>
          </Grid>
          <Grid item md={3} xs={12}>
            <TextField
              select
              fullWidth
              size="small"
              label="Регион"
              variant="outlined"
              value={selectRegion}
              onChange={(event) => setSelectRegion(event.target.value)}
            >
              <MenuItem value="">
                Без фильтра
              </MenuItem>
              {filterListByRegion.map((item) => (
                <MenuItem value={item} key={item}>
                  {item}
                </MenuItem>
              ))}
            </TextField>
          </Grid>
          <Grid item md={3} xs={12}>
            <TextField
              label="Город"
              variant="outlined"
              fullWidth
              size="small"
              onChange={(event) => setFieldCity(event.target.value)}
            />
          </Grid>
        </Grid>

        <Box ml={2}>
          <IconButton aria-label="Обновить" onClick={getCompaniesList}>
            <RefreshIcon />
          </IconButton>
        </Box>
      </Box>

      <ClientsTable
        list={companiesFiltered}
        count={companies.count}
        page={companyPage}
        changePage={handleChangePage}
        onDelete={deleteCompany}
      />
    </>
  );
}
