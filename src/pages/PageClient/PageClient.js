import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Box, Tabs, Tab } from '@material-ui/core';
import { toast } from 'react-toastify';
import PropTypes from 'prop-types';
import { companyService } from '../../api/company.service';
import { Loading, FormEditClient, ClientBankDetailsTable } from '../../components';

function TabPanel({ children, value, index }) {
  return (
    <div>
      {value === index && (
        <Box mt={6}>
          {children}
        </Box>
      )}
    </div>
  );
}
TabPanel.propTypes = {
  children: PropTypes.node.isRequired,
  index: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
};

export function PageClient() {
  const { id } = useParams();
  const [initialFields, setInitialFields] = useState(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    companyService.companyRead(id)
      .then(({ data }) => {
        setInitialFields(data);
      })
      .catch(() => toast.error('Не удалось загрузить данные, попробуйте обновить страницу'));
  }, [id]);

  const editClient = (fields) => {
    setLoading(true);

    companyService.companyUpdate(id, fields)
      .then(({ data }) => {
        setInitialFields({ ...initialFields, ...data });
        toast.success('Успешно обновлено');
      })
      .catch(() => toast.error('Не удалось обновить.'))
      .finally(() => setLoading(false));
  };

  const updateBankDetails = (data) => setInitialFields({ ...initialFields, bank_details: data });

  const [tab, setTab] = useState(0);
  const handleChangeTab = (_, newTab) => setTab(newTab);

  return (
    <>
      {initialFields === null ? (
        <Loading />
      ) : (
        <Box boxShadow={2} p={4}>
          <Tabs
            value={tab}
            onChange={handleChangeTab}
            indicatorColor="primary"
            textColor="primary"
            variant="scrollable"
            scrollButtons="auto"
          >
            <Tab label="Информация" />
            <Tab label="Банковские реквизиты" />
          </Tabs>

          <TabPanel value={tab} index={0}>
            <FormEditClient
              initialFields={initialFields}
              onSubmit={editClient}
              isLoading={loading}
            />
          </TabPanel>

          <TabPanel value={tab} index={1}>
            <ClientBankDetailsTable
              companyPk={id}
              initialFields={initialFields.bank_details}
              onSubmit={updateBankDetails}
            />
          </TabPanel>
        </Box>
      )}
    </>
  );
}
