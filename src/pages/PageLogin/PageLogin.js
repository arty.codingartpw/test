import React, { useContext, useState } from 'react';
import { Box } from '@material-ui/core';
import { toast } from 'react-toastify';
import { FormLogin } from '../../components';
import { authService } from '../../api/auth.service';
import { AuthContext } from '../../contexts/AuthContext';

export function PageLogin() {
  const [loading, setLoading] = useState(false);
  const { setIsLoggedIn } = useContext(AuthContext);
  const login = ({ email, password }) => {
    setLoading(true);
    authService.login(email, password)
      .then(() => {
        setIsLoggedIn(true);
      })
      .catch(() => {
        toast.error('Ошибка авторизации');
        setLoading(false);
      });
  };

  return (
    <Box display="flex" justifyContent="center" alignItems="center" height="100vh">
      <FormLogin onSubmit={login} isLoading={loading} />
    </Box>
  );
}
